class GlassesList {
  constructor() {
    this.arr = [];
  }
  addGlasses(glasses) {
    this.arr.push(glasses);
  }
  renderGlasses() {
    let content = "";
    content = this.arr.reduce((glContent, item, index) => {
      glContent += `
            <div class="col-4">
                <img onclick="tryOnGlasses(event)" data-index="${index}" class="img-fluid" src="${item.src}" />
            </div>
        `;
      return glContent;
    }, "");
    return content;
  }
  putOnGlasses(index) {
    return `<img id="glassesImg" src="${this.arr[index].virtualImg}" />`;
  }
  showInfo(index) {
    let { name, brand, color, price, status, description } = this.arr[index];
    let content = `
        <h5>${name} - ${brand} - ${color}</h5>
        <p class="card-text">
            <span class="btn btn-danger btm-sm mr-2">${price}$</span>
            <span class="text-success">${status}</span>
        </p>
        <p>${description}</p>
    `;
    return content;
  }
}
export default GlassesList;
